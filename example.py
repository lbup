#!/usr/bin/python3

import argparse
import logging
import sys

from lbup.repository import Repo
from lbup.ssh_remote import SSHRemote
from lbup.targets    import TargetLocal, TargetSSH, TargetSSHLXCLVM

def list_targets(tgts):
    for tgt in tgts:
        sys.stdout.write('%s\n  %s\n' % (tgt.name, str(tgt)))

# parse the commandline
parser = argparse.ArgumentParser()
parser.add_argument('-l', '--list-targets', action = 'store_true')
parser.add_argument('-n', '--dry-run', action = 'store_true')
parser.add_argument('-v', '--verbose', action = 'count', default = 0)
parser.add_argument('targets', nargs = argparse.REMAINDER)
args = parser.parse_args()

# configure logging
logging.basicConfig(level = max(3 - args.verbose, 0) * 10)
# define the backup targets
tgts = (
    #TargetLocal('local', dirs = ['/usr/local/share']),
)


if args.list_targets:
    list_targets(tgts)
    sys.exit(0)

if len(args.targets) > 0:
    tgts_run = []
    for requested in args.targets:
        try:
            tgts_run.append(next(filter(lambda tgt: tgt.name == requested, tgts)))
        except StopIteration:
            sys.stderr.write('Requested target "%s" not defined\n' % requested);
            sys.exit(1)
else:
    tgts_run = tgts

repo = Repo()
res = repo.backup(tgts_run, dry_run = args.dry_run)
for tgt, tgt_res in res.target_results.items():
    if tgt_res.success:
        continue

    logging.error('Backing up target "%s" failed', tgt)
    if tgt_res.output:
        output = tgt_res.output.decode('utf-8', errors = 'backslashreplace')
        output = '\n'.join(('  ' + line for line in output.splitlines()))
        logging.error('Target output:')
        logging.error('%s', output)

if not res.par2_result.success:
    sys.stderr.write('Generating par2 recovery information failed with code %d, bup output:\n' %
                     (res.par2_result.retcode))
    sys.stderr.write(res.par2_result.output.decode('utf-8', errors = 'backslashreplace') + '\n')
