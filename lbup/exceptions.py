class BackupException(Exception):
    pass

class RemoteExecException(BackupException):
    retcode = None
    output  = None
    def __init__(self, explanation, retcode, output):
        super().__init__(explanation)
        self.retcode = retcode
        self.output  = output

    def __str__(self):
        return (super().__str__() +
                ';%d: %s' % (self.retcode, self.output.decode('utf-8', errors = 'backslashreplace')))
