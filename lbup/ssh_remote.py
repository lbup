class SSHRemote:
    """
    Specification of an SSH remote host, represented by a combination of host,
    port and username, plus an optional proxy remote.
    :param str host:
    :param int port:
    :param str username:
    :param SSHRemote proxy_remote: proxy through which the connection should be
        tunnelled
    """
    host         = None
    port         = None
    username     = None
    proxy_remote = None
    def __init__(self, host, port, username, proxy_remote = None):
        self.host         = host
        self.port         = port
        self.username     = username
        self.proxy_remote = proxy_remote
    def __str__(self):
        return '{user}@{host}:{port}{proxy}'.format(
                user = self.username,
                host = self.host,
                port = self.port,
                proxy = '@@{%s}' % str(self.proxy_remote) if self.proxy_remote else '')
